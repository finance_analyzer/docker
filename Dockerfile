FROM ruby:2.6.5-alpine

ENV \
  DEV_PACKAGES="bash ruby-dev build-base postgresql-dev tzdata git nodejs yarn" \
  APP_HOME="/app" \
  CHROMIUM_DRIVER_VERSION="2.46"

WORKDIR $APP_HOME

RUN \
  rm -rf /app && \
  mkdir -p /app/tmp/pids /app/tmp/cache /app/tmp/sockets

# Установка зависимостей
RUN set -ex && \
  apk --update --upgrade add --no-cache $DEV_PACKAGES && \
  rm -rf /var/cache/apk/* && \
  echo ‘gem: --no-document’ > /etc/gemrc && \
  gem install bundler

# Install chromium for chrome driver
RUN apk add --no-cache chromium

# Install chrome driver for cuprite
RUN wget -O /tmp/chromedriver.zip http://chromedriver.storage.googleapis.com/$CHROMIUM_DRIVER_VERSION/chromedriver_linux64.zip \
    && unzip /tmp/chromedriver.zip chromedriver -d /usr/bin/ \
    && rm /tmp/chromedriver.zip \
    && chmod ugo+rx /usr/bin/chromedriver

RUN echo /usr/bin/chromium-browser --product-version

# Установка часового пояса
RUN cp /usr/share/zoneinfo/Europe/Moscow /etc/localtime && \
    echo "Europe/Moscow" > /etc/timezone

# Установка гемов
COPY Gemfile* ./
RUN bundle install

# Установка пакетов из веб пака
COPY package.json ./
COPY yarn.lock ./
RUN yarn install --check-files
